const bcrypt = require('bcrypt')
const authConfig = require('./token').authConfig
const jwt = require('jsonwebtoken')
const usermodel = require('../models/user.entity')
const adminmodel = require('../models/admin.entity')

const comparePassword=(givenPw,savePw,cb)=>{
    bcrypt.compare(givenPw,savePw,(err,isMatch)=>{
        if (err) cb(err)
        cb(null,isMatch)
    })
}
//token genaration 
const generateToken = (payload,done) => {
    jwt.sign(payload,authConfig.jwtSecret,{expiresIn:'1h'},done)
}
//verifying the token 
const varifyToken = (req,res,next) => {
    let {authorization} = req.headers
    if(!authorization){
        return res.status(401).send({message:"user must be login"})
    }
    let token = authorization.replace("Bearer ","")
    jwt.verify(token,authConfig.jwtSecret,(err,payload)=>{
        if(err){
            return res.status(401).send({message:"user must be login",error:err})
        }
        const {_id}= payload
        usermodel.findOne({_id:_id})
        .then(user=>{
        })
        next()
    })
}
const adminVarifyToken = (req,res,next) => {
    let {authorization} = req.headers
    if(!authorization){
        return res.status(401).send({message:"you must be login"})
    }
    let token = authorization.replace("Bearer ","")
    jwt.verify(token,authConfig.jwtSecret,(err,payload)=>{
        if(err){
            return res.status(401).send({message:"you must be login",error:err})
        }
        const {_id}= payload
        adminmodel.findOne({_id:_id})
        .then(user=>{
        })
        next()
    })
}
module.exports = {
    comparePassword,
    generateToken,
    varifyToken,
    adminVarifyToken
}