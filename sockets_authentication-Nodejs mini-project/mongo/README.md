# welcome to Authentication & Sockets project developed by ©Naveen kumar donka., wipro mean c1
    -name of the database is socket.Having users and admins.
    -users,admin able to perform the register,login and logout using the jsonwebtoken.
    -password for both user and admin are encrypted by using the bcrypt module.
    -before opening the url (protected routes) add headers to authenticate the user.They are protected by using the JsonWebToken.
    -entity of the user and admin are below
    -admin entity
        {
            adminid:Number,
            adminname:String,
            password:String
        }
    -user entity
        {
            userid:Number,
            username:String,
            password:String
        }

# 1.1 signin,signout and register routes for user

    1 Register user
        -method : post
        -path : http://localhost:3000/user/register
        -it requires the body. Body consistes of the usermodel || userid must be unique
        
    2 signin user
        -method : post
        -path : http://localhost:3000/user/signin
        -it requires the body. Body consistes of the usermodel
        -if the user is valid it will genarate a jwt token.It will be used for authentication

    3 signout user
        -method : get 
        -path : http://localhost:3000/user/signout
    
# 1.2 signin,signout and register routes for admin

    1 Register admin
        -method : post
        -path : http://localhost:3000/admin/register
        -it requires the body. Body consistes of the adminmodel || adminid must be unique
        
    2 signin admin
        -method : post
        -path : http://localhost:3000/admin/signin
        -it requires the body. Body consistes of the adminmodel
        -if the user is valid it will genarate a jwt token.It will be used for authentication

    3 signout admin
        -method : get 
        -path : http://localhost:3000/admin/signout

# 2 Broadcasting the message sended by admin  to all the active users

    -the jwt token will be verified. if the token is valid then it will give you access to recive the message.
    -the users can be connected to the server by using get method
    - use the body to share the data and messages always use Raw/Json
    -the route is protected with JsonWebToken please pass the token in the postman header
    -1st open one or more users (get method) in the postman and later  open the admin (post method ) to send the data.

        key : Authorization
        value : Bearer token-genarated-by-jwt

        EX :: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmMwMDdhZWViN2YxNTZhYjk5MWUzZjQiLCJpYXQiOjE2NTY3NTIwNjgsImV4cCI6MTY1Njc1NTY2OH0.m4P6H3sbWvuM2dN-5DXWTMu_d6n3xXUxP8taQNk0EC0

    -1st open the user getchat route in get method wait for the data
    -method : get
    -url : http://localhost:3000/user/broadcastchat

    ---------------      open the url in many tabs here 1 tab in postman = 1 user 

    -the admin should be connected to server to send the broadcast message
    -method : post
    -url : http://localhost:3000/admin/broadcast
    -message show should be shared by using the body in the form of JSON format.
        -ex :  {    "message":"hello users this is admin"     }

    ---------------     if the message is posted then go to the get methods or users the message will be reflected there.

# 3 one to one chating with the active users

    -the users show join in the room to chat one-to-one.
    -using the postaman it is very hard to get data in the live that's why the output is printed using the console.
    -before opening the url add headers to authenticate the user
    
        key : Authorization
        value : Bearer token-genarated-by-jwt

        EX :: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmMwMDdhZWViN2YxNTZhYjk5MWUzZjQiLCJpYXQiOjE2NTY3NTIwNjgsImV4cCI6MTY1Njc1NTY2OH0.m4P6H3sbWvuM2dN-5DXWTMu_d6n3xXUxP8taQNk0EC0

    -body should be in the below format

        {
            "roomname":"Wipro",
            "username":"naveen",
            "message":"hello i am naveen"
        }
    
    method : post 
    url : http://localhost:3000/user/chatroom

    -the output is printed in the console visit the console once.
    -along with i have developed a one-to-one live chat system in the separate folder it can be done by using the UI.
