const router = require('express').Router()
const bcrypt= require('bcrypt')
const auth = require('../../middleware/index')
const adminmodel = require('../../models/admin.entity') 

router.post('/register',(req,res)=>{
    let {adminId,adminname,password}=req.body
    bcrypt.hash(password,12)
    .then((passhash)=>{
        password = passhash
        // console.log(password);
        let newAdmin = new adminmodel({adminId,adminname,password})
        newAdmin.save((error,newAdmin)=>{
            if(error){
                res.send({message:error})
            }
            else
            {
                res.send({message:"adnin added"})
            }
        })
    })    
})

router.post('/signin',(req,res)=>{
    const val=req.body
    adminmodel.findOne({adminname:val.adminname})
    .then((user)=>{
        if (user) {
            auth.comparePassword(val.password, user.password, (error, isMatch) => {
                if (isMatch && !error) {
                    console.log(user);
                    let payload = {_id:user._id}
                    auth.generateToken(payload,(err,token)=>{
                        res.send({message:"welcome admin",token:token})
                    })
                    // res.send({ message: "admin logged in successfully"})
                }
                else {
                    res.send({ message: "Invalid admin details. Please enter valid admin name and password " })
                }
            })
        }
        else {
            res.send({ message: "Please enter valid admin name and password " })
        }
})
})
router.get('/signout',(req,res)=>{
    res.send({message:"admin logout sucessfully"})    
})

router.post('/broadcast',auth.adminVarifyToken,(req,res)=>{
    const socket = require("socket.io-client")("ws://localhost:3000/")
    socket.on("connection",() => {
        console.log("Client is Connected")
    })
    let msg = req.body
    socket.emit('new',msg)
    res.send({message:"welcome admin",broadcasted_message_is:msg})

})
module.exports=router