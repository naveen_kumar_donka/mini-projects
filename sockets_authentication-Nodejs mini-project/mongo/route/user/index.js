const router = require('express').Router()
const bcrypt = require('bcrypt')
const auth = require('../../middleware/index')
const usermodel=require('../../models/user.entity')
// const socket = require("socket.io-client")("http://localhost:3000/")

router.post('/register',(req,res)=>{
    let {userid,username,password}=req.body
    bcrypt.hash(password,12)
    .then((hashpass)=>{
        password=hashpass
        let newUser = new usermodel({userid,username,password})
        newUser.save((error,newUser)=>{
            if(error){
                res.send({message:error})
            }
            else{
                res.send({message:"user added sucessfully"})
            }
        })
    })   
})

router.post('/signin',(req,res)=>{
    const val=req.body
    usermodel.findOne({username:val.username})
    .then((user)=>{
        if (user) {
            auth.comparePassword(val.password, user.password, (error, isMatch) => {
                if (isMatch && !error) {
                    let payload = {_id:user._id}
                    auth.generateToken(payload,(err,token)=>{
                        res.send({message:"welcome user",token:token})
                    })
                }
                else {
                    res.send({ message: "Invalid user details. Please enter valid userName and password " })
                }
            })
        }
        else {
            res.send({ message: "Please enter valid userName and password " })
        }
})
})

router.get('/signout',(req,res)=>{
    res.send({message:"user sucessfully logout"})
})

router.get('/broadcastchat',auth.varifyToken,(req,res)=>{
    const socket = require("socket.io-client")("ws://localhost:3000")
    socket.on('news_b',(dat)=>{
        // console.log(dat);
        res.send({info:"broadcasted message",message_from_admin:dat})
    })
})

router.post('/chatroom',auth.varifyToken,(req, res, next) => {

    const socket = require("socket.io-client")("ws://localhost:3000")
    socket.on("connect", () => {
        console.log("user is Connected")
    })
    //In the body you need to  provide any username,roomname and message 

    const body = req.body
    socket.emit("join_room",body)
    socket.emit("send_message", body)
    res.send({room_name : body.roomname,user_name:body.username,messsage:body.message,visit_the_console:"message is posted visit the console once"})
    // res.send({message:"message is posted visit the console once"})
})

router.get('/chatroom',(req,res)=>{
    const socket = require("socket.io-client")("ws://localhost:3000")
    socket.on("connect", () => {
        console.log("user is Connected")
    })
        socket.on("send_message",(err,message)=>{
            res.send({info:"message sended by the user ",message_from_user:message})
        })
})

module.exports = router