const mongose = require('mongoose')

const createMongoConnection = () =>{
    mongose.connect("mongodb://localhost/books")
}
const getMongoConnection = () =>{
    return mongose.connection;
}
const onError = (err) =>{
    console.log("Error while connecting to the DATABASE");
}
const onSuccess = () => {
    console.log("connected to databse dbname : books");
}
module.exports = {
    createMongoConnection,
    getMongoConnection,
    onError,
    onSuccess
}