const express = require('express')
const app = express()
const database=require('./db-connection')
const server=require('http').createServer(app)
const io=require('socket.io')(server)
app.use(express.json())

io.on("connection",(socket)=>{
    // console.log("socket is ",socket.id);
    socket.on('new',(data)=>{
        socket.broadcast.emit('news_b',data)  
    })
})

io.on("connect",(socket)=>{
    // console.log("socket is ",socket.id);
socket.on('join_room', (body)=> {
    // socket.join(body.roomname)
    console.log("user is joined in the room :",body.roomname)
});
socket.on('send_message', (body)=> {
    socket.to(body.roomname).emit("receive message",body.message)
    console.log("-------------------------------------------------------");
    console.log(`****** << message send in the room : ${body.roomname} >> ******`)   
    console.log(`room_name : ${body.roomname}`);
    console.log(`user_name : ${body.username}`);
    console.log(`message_from_user : ${body.message}`);
    console.log("-------------------------------------------------------");
});
})

database.createMongoConnection()
const db=database.getMongoConnection()
db.on('error',database.onError)
db.on('open',database.onSuccess)

app.use('/user',require('./route/user'))
app.use('/admin',require('./route/admin'))
app.use('/',(req,res)=>{
    res.status(200).send("welcome to Authentication & Sockets project developed by naveen donka")
})

module.exports= server