const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const Scheme = mongoose.Schema

const userSchema = new Scheme({
    userid:{
        type:Number,
        unique:true,
        required:true
    },
    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
})
module.exports = mongoose.model('user',userSchema)