const router = require('express').Router()

const book=require('./book.entity')


// books shown to user without login route
router.get('/',(req,res,next)=>{
    book.find()
    .then(result=>{
        res.status(200).json({
            booksdata:result
        });
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        })
    })
})
 
module.exports = router