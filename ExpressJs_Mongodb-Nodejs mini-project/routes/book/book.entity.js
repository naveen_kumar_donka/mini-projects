const mongoose =require('mongoose')
// const {ObjectId}=mongoose.Schema.Types
const Schema =mongoose.Schema
const bookSchema = new Schema({
    // _id:mongoose.Schema.Types.ObjectId,
    bookId:{
        type:String,
        unique:true,
        require:true
    },
    bookName:{
        type:String,
        unique:true,
        require:true
    },
    bookAuthor:{
        type:String,
        require:true
    },
    // like:[{type:ObjectId,ref:"user"}],
    // readLater:[{type:ObjectId,ref:"user"}]
    
})

const Book = mongoose.model('book',bookSchema);
module.exports=Book;