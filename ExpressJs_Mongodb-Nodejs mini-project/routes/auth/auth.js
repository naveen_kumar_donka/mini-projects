let bcrypt =require('bcrypt')

const comparePassword=(givenPw,savePw,cb)=>{
    bcrypt.compare(givenPw,savePw,(err,isMatch)=>{
        if (err) cb(err)
        cb(null,isMatch)
    })
}

module.exports = {
    comparePassword
}