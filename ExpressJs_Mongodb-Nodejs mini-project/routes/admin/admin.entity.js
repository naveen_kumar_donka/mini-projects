const bcrypt= require('bcrypt');
const mongoose =require('mongoose')
const Schema =mongoose.Schema

const adminSchema = new Schema({
    // _id:mongoose.Schema.Types.ObjectId,
    adminId:{
        type:String,
        require:true
    },
    adminName:{
        type:String,
        unique:true,
        require:true
    },
    password:{
        type:String,
        require:true
    }
    
})

adminSchema.pre('save',function(next){
    let admin = this;
    if(this.isModified('password')|| this.isNew){
        bcrypt.genSalt(10,function(err,salt){
            if(err){
                next(err);
            }
            bcrypt.hash(admin.password,salt,function(err,hash){
                if(err){
                    next(err)
                }
                admin.password = hash;
                next();
            })
        })
    }else{
       next();
    }
})

module.exports = mongoose.model('admin',adminSchema);