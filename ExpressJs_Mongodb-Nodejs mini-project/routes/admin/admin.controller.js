const adminDao = require('./admin.dao')
var currentAdmin;
const addAdmin = (req, res) => {
    const admin = req.body
    adminDao.addAdmin(admin, (err, response) => {
        if (err) res.status(500).send({ error: "something is wrong" })
        else res.status(201).send(response);
    })
}
const loginAdmin = (req,res) => {
    const admin = req.body
    adminDao.loginAdmin(admin)
    .then(data => {
       currentUser = data.admin
        res.send(data.message)
      
    })
    .catch(err => {
        res.send(err)
    })
}
const logoutAdmin = (req,res) => {
    currentAdmin = null;
    res.send(`Admin is logout successfull`)
}

const addBook = (req, res) => {
    const book = req.body
    adminDao.addBook(book, (err, response) => {
        if (err) res.status(500).send({ error: "something is wrong" })
        else res.status(201).send(response);
    })
}
module.exports = {
    addAdmin,
    loginAdmin,
    logoutAdmin,
    addBook
}