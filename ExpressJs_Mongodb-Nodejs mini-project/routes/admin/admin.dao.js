const adminModule = require('./admin.entity')
const bookModule =require('../book/book.entity')
const auth = require('../auth');
const addAdmin = (adminInfo, done) => {
    let newAdmin = new adminModule();
    // newAdmin._id=adminInfo
    newAdmin.adminId = adminInfo.adminId;
    newAdmin.adminName=adminInfo.adminName;
    newAdmin.password = adminInfo.password;
    newAdmin.save((err, admin) => {
        if (err) done(err)
        else done(null, {
            message: 'admin registerd successfully',
            addedAdmin: admin
        })
    })

}
const addBook = (bookInfo, done) => {
    let newBook = new bookModule();
    newBook.bookId = bookInfo.bookId;
    newBook.bookName=bookInfo.bookName;
    newBook.bookAuthor = bookInfo.bookAuthor;
    newBook.save((err, book) => {
        if (err) done(err)
        else done(null, {
            message: 'book is added',
            addedBook: book
        })
    })

}

const loginAdmin = (adminInfo) => {
    return new Promise((resolve, reject) => {
        adminModule.findOne({ adminName: adminInfo.adminName }, (err, admin) => {
            if (admin) {
                auth.comparePassword(adminInfo.password, admin.password, (error, isMatch) => {
                    if (isMatch && !error) {

                        resolve({ message: "admin logged in successfully", admin: admin })

                    }
                    else {
                        reject({ message: "Invalid admin details. Please enter valid adminName and password " })
                    }
                })
            }
            else {
                reject({ message: "invalid admin details" })
            }
        })
    })
}

module.exports = {
    addAdmin,
    loginAdmin,
    addBook
}