const router = require('express').Router()
const adminCtrl = require('./admin.controller')
const book = require('../book/book.entity')
const user = require('../user/user.entity')
//admin register
router.post('/register', adminCtrl.addAdmin)
//login admin
router.post('/login', adminCtrl.loginAdmin)
//log out admin
router.get('/logout', adminCtrl.logoutAdmin)

// curd operation on books my admin
//add book by admin
router.post('/addbook', adminCtrl.addBook)

//show all book to admin
router.get('/book', (req, res, next) => {
    book.find()
        .then(result => {
            res.status(200).json({
                booksdata: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
})
//show specific book to admin by targeting specific book mongodb generated object_id 
router.get('/book/:id', (req, res, next) => {
    book.findOne({_id: req.params.id})
        .then(result => {
            res.status(200).json({
                Book_details: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
})
//update book by admin by targeting specific book  mongodb generated object_id 
router.put('/book/:id', (req, res, next) => {
    book.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
            bookId:req.body.bookId,
            bookName:req.body.bookName,
            bookAuthor:req.body.bookAuthor
        }
    })
        .then(result => {
            res.status(200).json({
                message: 'book updated',
            })
        })
        .catch(err => {
            console.log(err);
        })
})
// delete book by admin by targeting specific book  mongodb generated object_id 
router.delete('/book/:id', (req, res, next) => {
    // console.log(req.params.id);
    book.remove({ _id: req.params.id })
        .then(result => {
            res.status(200).json({
                message: 'book deleted',
                result: result
            })
        })
        .catch(err => {
            console.log(err);
        })
})

//curd on users by admin
//show all users to admin
router.get('/user', (req, res, next) => {
    user.find()
        .then(result => {
            res.status(200).json({
                usersdata: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
})
//show specific user to admin by targeting specific user mongodb generated object_id 
router.get('/user/:id', (req, res, next) => {
    user.findOne({_id: req.params.id})
        .then(result => {
            res.status(200).json({
                User_details: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
})
//update user by admin by targeting specific user mongodb generated object_id 
router.put('/user/:id', (req, res, next) => {
    user.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
            userId:req.body.userId,
            userName:req.body.userName,
            password:req.body.password
        }
    })
        .then(result => {
            res.status(200).json({
                message: 'user updated',
            })
        })
        .catch(err => {
            console.log(err);
        })
})
//delete user by admin by targeting specific user mongodb generated object_id 
router.delete('/user/:id', (req, res, next) => {
    // console.log(req.params.id);
    user.remove({ _id: req.params.id })
        .then(result => {
            res.status(200).json({
                message: 'this user deleted',
                result: result
            })
        })
        .catch(err => {
            console.log(err);
        })
})
module.exports = router