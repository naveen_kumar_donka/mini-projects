const userDao = require('./user.dao')
var currentUser;
const addUser = (req, res) => {
    const user = req.body
    userDao.addUser(user, (err, response) => {
        if (err) res.status(500).send({ error: "something is wrong" })
        else res.status(201).send(response);
    })
}
const login = (req,res) => {
    const user = req.body
    userDao.loginUser(user)
    .then(data => {
       currentUser = data.user
        res.send(data.message)
      
    })
    .catch(err => {
        res.send(err)
    })
}
const logOut = (req,res) => {
    currentUser = null;
    res.send(`User logged out successfully`)
}

module.exports = {
    addUser,
    login,
    logOut
}