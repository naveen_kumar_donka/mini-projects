const userModule = require('./user.entity')
const auth = require('../auth');
const addUser = (userInfo, done) => {
    let newUser = new userModule();
    newUser.userId = userInfo.userId;
    newUser.userName = userInfo.userName;
    newUser.password = userInfo.password;
    newUser.save((err, user) => {
        if (err) done(err)
        else done(null, {
            message: 'User registered successfully',
            addedUser: user
        })
    })

}

const loginUser = (userInfo) => {
    return new Promise((resolve, reject) => {
        userModule.findOne({ userName: userInfo.userName }, (err, user) => {
            if (user) {
                auth.comparePassword(userInfo.password, user.password, (error, isMatch) => {
                    if (isMatch && !error) {

                        resolve({ message: "User logged in successfully", user: user })

                    }
                    else {
                        reject({ message: "Invalid user details. Please enter valid userName and password " })
                    }
                })
            }
            else {
                reject({ message: "Please enter valid userName and password " })
            }
        })
    })
}




module.exports = {
    addUser,
    loginUser
}