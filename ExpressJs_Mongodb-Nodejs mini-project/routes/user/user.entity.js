const bcrypt= require('bcrypt');
const mongoose =require('mongoose')
const Schema =mongoose.Schema
// const {ObjectId}=mongoose.Schema.Types
const UserSchema = new Schema({
    // _id:mongoose.Schema.Types.ObjectId,
    userId:{
        type:String,
        require:true
    },
    userName:{
        type:String,
        unique:true,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    like:[],
    readLater:[]
})

UserSchema.pre('save',function(next){
    let user = this;
    if(this.isModified('password')|| this.isNew){
        bcrypt.genSalt(10,function(err,salt){
            if(err){
                next(err);
            }
            bcrypt.hash(user.password,salt,function(err,hash){
                if(err){
                    next(err)
                }
                user.password = hash;
                next();
            })
        })
    }else{
       next();
    }
})

const user = mongoose.model('user',UserSchema);
module.exports=user;

// module.exports = mongoose.model('user',UserSchema);