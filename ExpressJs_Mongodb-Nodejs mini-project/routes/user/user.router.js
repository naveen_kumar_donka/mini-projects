const router = require('express').Router();
const userCtrl = require('./user.controller')
const book = require('../book/book.entity');
const user = require('./user.entity')


// user register route
router.post('/register', userCtrl.addUser);
// user log in route
router.post('/login', userCtrl.login);
//user log out route
router.get('/logout', userCtrl.logOut);

// book shown route to user after login
router.get('/book', (req, res, next) => {
    book.find()
        .then(result => {
            res.status(200).json({
                booksdata: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
})
// pushing book to like array by updating schema  using this route here ":id" is of specific user which is targated 
router.put('/like/:id', (req, res, next) => {
    user.findOneAndUpdate({ _id: req.params.id }, {
        // user founded and book is pushed into like array
        $push: {
            like: req.body
        }
    })
        .then(result => {
            res.status(200).json({
                message: 'book added to like section',
            })
        })
        .catch(err => {
            console.log(err);
        })
})
// trying to show liked books section of particular user by checking the monogodb created object _id
// here ":id" is of specific user which is targated 
router.get('/like/:id', (req, res, next) => {
    user.findOne({ _id: req.params.id }, {
    })
        .then(result => {
            res.status(200).json({
            Liked_Book: result.like,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })


})
// pushing book to readlater array by updating schema using this route here ":id" is of specific user which is targated 
router.put('/readlater/:id', (req, res, next) => {
    user.findOneAndUpdate({ _id: req.params.id }, {
        // user founded and book is pushed into readlater array
        $push: {
            readLater:
                req.body
        }
    })
        .then(result => {
            res.status(200).json({
                message: 'book added to readlater section',
            })
        })
        .catch(err => {
            console.log(err);
        })
})


//trying to show readlater books section of particular user by checking the monogodb created object _id
// here ":id" is of specific user which is targated
router.get('/readlater/:id', (req, res, next) => {
    user.findOne({ _id: req.params.id }, {
    })
        .then(result => {
            res.status(200).json({
                Read_Later_Book: result.readLater,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
})
module.exports = router;