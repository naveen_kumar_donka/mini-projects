let mongoose =require('mongoose')

const createMongoConnection=()=>{
    mongoose.connect('mongodb://localhost/Bookess')
}
const getMongoConnection=()=>{
    return mongoose.connection
}

const onError =(error)=>{
    console.log(error)
}
const onSuccess =()=>{
    console.log("Connection established db name :{Bookess}")
}
module.exports = {
    createMongoConnection,
    getMongoConnection,
    onError,
    onSuccess
}