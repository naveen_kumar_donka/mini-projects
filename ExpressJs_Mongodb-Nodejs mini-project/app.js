const express = require('express')
const app = express()
const api = require('./routes')
const bodyParser= require('body-parser')
const connection = require('./connection')

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

connection.createMongoConnection();
const dbConnection = connection.getMongoConnection()
dbConnection.on('error',connection.onError);
dbConnection.on('open',connection.onSuccess);

// default route to show book in book store 
app.use('/api',api)
module.exports = app
