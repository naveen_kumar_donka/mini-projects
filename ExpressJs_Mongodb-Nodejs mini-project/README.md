# Developed By Donka Naveen Kumar Wipro MEAN - C1 
  ## General info about the database,server and models.
        - server listening at localhost 3030 you can change the port to in /bin/www
        - database name is "Bookess". 3 collections in the database 1.users 2.admins 3.books
        - encrypted the user and admin password in the database by using the bcrypt module.
        -always use the mongo genarated ID in there is an id place like add books,delete user....
# user schema model
       {
        "userId": 1,
        "userName": "username",
        "password": "password"
        }
# admin schema model
        {
        "adminId": 1,
        "adminName": "adminname",
        "password": "password"
        }
# book schema model
        {
        "bookId": 1,
        "bookName": "bookname",
        "bookAuthor": "authorname"
        }

### this is the purpose of good understanding of the Backend Application. 

## Show books to user without login.admin have to add books first before viewing.
        GET--- http://localhost:3000/api      
        -fetch all the books without any user login by default there is no books it's empty the admin needs to add the books


####                                                          Admin Register,Login,logout

  # 1 will register the admin to admin collection
        
        Post - http://localhost:3030/api/admin/register  
        -Alert its a POST request require body

  # 2 login admin by verifying the data the admin collection

        POST - http://localhost:3030/api/admin/login 
        -Alert its a POST request require body i.e adminName and password are must

  # 3 logout the admin
        GET - http://localhost:3030/api/admin/logout  


#                                                              User Register,Login,logout

  # 1 will register the user to user collection
        POST - http://localhost:3030/api/user/register  
        -Alert its a POST request require body

  # 2 login the user after verfying the user collection
        POST - http://localhost:3030/api/user/login 
        -Alert its a POST request require body i.e userName and password are must

  # 3 logout the user
        GET - http://localhost:3030/api/user/logout  
    
###                             Admin CURD on Books i.e Add_book,Show_book,Update_book,Delete_book

  # add the book to books collection 
        POST - http://localhost:3030/api/admin/addbook 
        -Alert its a POST request require body

  # show the all the book from books collection to admin
        GET - http://localhost:3030/api/admin/book

  # Show the specific book(target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific book
        GET - http://localhost:3030/api/admin/book/:id 

  # update the specific book(target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific book
        PUT - http://localhost:3030/api/admin/book/:id 
        -Alert its a PUT request require body parameter which you want to update

  # delete the specific book(target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific book
        DELETE - http://localhost:3030/api/admin/book/:id 
        To see new book collection again go to {GET--- http://localhost:3000/api/admin/book}


###                                     Admin CURD on users i.e Show_user,Update_user,Delete_user

  # show all the user from user collection to admin
        GET - http://localhost:3030/api/admin/user

  # show the specific user(target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific user
        GET - http://localhost:3030/api/admin/user/:id 

  # update the specific user(target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific user
        PUT - http://localhost:3030/api/admin/user/:id 
        -Alert its a PUT request require body parameter which you want to update

  # delete the specific user(target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific user
        DELETE - http://localhost:3030/api/admin/user/:id 
        - To see new user collection again go to {GET--- http://localhost:3000/api/admin/user}


###                             operation on  User to add_to_like, add_to_readlater, see like & readlater section of

  # show all the book from book collection again to user
        GET--- http://localhost:3030/api/user/book

  # add the book to like section of specific user(user target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific user
        PUT - http://localhost:3030/api/user/like/:id
        - its a PUT request 
        -IMPORTANT ":id" in url is user mongodb generated object "_id" of the specific user

  # show the like section of specific user(user target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific user
        GET - http://localhost:3030/api/user/like/:id
        -IMPORTANT ":id" in url is user mongodb generated object "_id" of the specific user
            
  # add the book to readLater section of specific user(user target by mongodb generated object "_id") so replace ":id" from url with mongodb generated "_id" of the specific user
        PUT - http://localhost:3030/api/user/readlater/:id
        -Alert its a PUT request 
        -IMPORTANT ":id" in url is user mongodb generated object "_id" of that specific user

  # fetch the Book by id
        GET - http://localhost:3030/api/user/readlater/:id
        -IMPORTANT ":id" in url is user mongodb generated object "_id" of the specific user




## Method used is "findOneAndUpdate" property is "$push" so it require books details in body to push the details into "like" array of user schema
